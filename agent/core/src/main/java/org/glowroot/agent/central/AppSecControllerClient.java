/*
 *  Copyright (c) 2018 Colortokens, Bangalore
 *
 *  All rights reserved
 *
 *  http://www.colortokens.com/
 *
 *  The source code in this file is licensed and does not fall under any  type of
 *  open source license.  No form of reproduction is allowed without  prior
 *  written consent from Colortokens.
 */

package org.glowroot.agent.central;

import com.colortokens.appsec.controller.api.model.AgentAPIGrpc;
import com.colortokens.appsec.controller.api.model.AgentAPIGrpc.AgentAPIBlockingStub;
import com.colortokens.appsec.controller.api.model.AgentHeartBeat;
import com.colortokens.appsec.controller.api.model.AgentRegistration.AgentRegistrationRequest;
import com.colortokens.appsec.controller.api.model.AgentRegistration.AgentRegistrationResponse;
import io.grpc.ManagedChannel;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;

import javax.net.ssl.SSLException;
import java.io.File;

/*
 *  Created by Aride Chettali on 2018-06-21.
 */

public class AppSecControllerClient
{

    private  AgentAPIBlockingStub blockingStub;

    public AppSecControllerClient(String host, int port) throws SSLException
    {
        String certFileName = "cert.pem";
        ManagedChannel channel = NettyChannelBuilder.forAddress(host, port)
                .sslContext(GrpcSslContexts.forClient().trustManager(new File("/home/ctuser/git/colortokens/appsec-controller/build/distributions/controller-1.0.0-SNAPSHOT/config/cert/cert.pem")    ).build()
                ).build();
        
        blockingStub = AgentAPIGrpc.newBlockingStub(channel);

    }

    public static void main(String[] args) throws Exception
    {
        AppSecControllerClient controller = new AppSecControllerClient("demo.colortokens.com",9090);
			 AgentRegistrationRequest req = AgentRegistrationRequest.newBuilder().setTenantId("1234567890").build();
		        AgentRegistrationResponse response = controller.blockingStub.registerWithController(req);
                System.out.print(response.toString());

       
        // Sample Agent HeartBEat Req
        AgentHeartBeat.AgentHeartBeatRequest req2 = AgentHeartBeat.AgentHeartBeatRequest.newBuilder()
                .setAgentUuid("sdsdasf76834ijdf945edd1")
                .setAgentType(AgentHeartBeat.AgentHeartBeatRequest.AgentType.JAVA_AGENT)
                .setAgentVersion("1.0.0")
                .setHeartbeatTimeInGmt(234123)
                .build();

        AgentHeartBeat.AgentHeartBeatResponse response2 = controller.blockingStub.sendHeartBeat(req2);
        System.out.print(response2.toString());
    }
}
